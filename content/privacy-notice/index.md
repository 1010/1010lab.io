---
title: "Privacy Notice"
date: 2020-01-05T10:26:41-05:00
draft: false
summary: "1010 Privacy Notice"
hidden: true
---

# 1010 Privacy Policy

1010 is authored and maintained by Chris Simpkins.  This privacy policy will explain how I handle personal data on the 1010 website.

    
## Contents

- What data do I collect?
- Do I collect data from minors?
- How do I collect your data?
- How will I use your data?
- How do I store your data?
- Marketing
- What are your data protection rights?
- What are cookies?
- How do I use cookies?
- What types of cookies do I use?
- How to manage your cookies
- Privacy policies of other websites
- Changes to this privacy policy
- How to contact me

## What data do I collect?

I do not collect personal identification data or anonymous data about you.

## Do I collect information from minors?

I do not market to or collect data from children under 18 years of age.

## How do I collect your data?

I do not receive direct or indirect data about you.

## How will I use your data?

I do not possess or maintain data about you.

## How will I store your data?

I will not store your data.

## Marketing

I do not request information for marketing purposes and do not maintain marketing data about you.

## What are your data protection rights?

In some regions, such as the **European Economic Area**, you have rights that allow you greater access to and control over your personal information. These may include the following:

*The right to access* – You have the right to request copies of your personal data.

*The right to rectification* – You have the right to request that I correct any information you believe is inaccurate. You also have the right to request that I complete the information you believe is incomplete.

*The right to erasure* – You have the right to request that I erase your personal data, under certain conditions.

*The right to restrict processing* – You have the right to request that I restrict the processing of your personal data, under certain conditions.

*The right to object to processing* – You have the right to object to my processing of your personal data, under certain conditions.

*The right to data portability* – You have the right to request that I transfer the data that I have collected to another organization, or directly to you, under certain conditions.

If you make a request, I have one month to respond to you. If you would like to exercise any of these rights, please contact me on Mastodon at <https://fosstodon.org/@chris>.

If you are a resident of **California**, you are granted specific rights regarding access to your personal information.

California Civil Code Section 1798.83, also known as the “Shine The Light” law, permits users who are California residents to request and obtain, once a year and free of charge, information about categories of personal information (if any) we disclosed to third parties for direct marketing purposes and the names and addresses of all third parties with which we shared personal information in the immediately preceding calendar year. If you are a California resident and would like to make such a request, please submit your request in writing on Mastodon at <https://fosstodon.org/@chris>.

## Cookies

Cookies are text files placed on your computer to collect standard Internet log information and visitor behavior information. When you visit the 1010 website, I do not collect information from you through cookies or similar technology.

For further information, visit [allaboutcookies.org](https://allaboutcookies.org).

## How do I use cookies?

I do not use cookies on the 1010 website.

## How to manage cookies

You can set your browser not to accept cookies, and the above website tells you how to remove cookies from your browser. The 1010 website is designed to deliver full functionality in the absence of cookie data.

## Privacy Policies of other websites

The 1010 website contains links to other websites. This privacy policy applies only to the 1010 website, so if you click on a link to another website, you should read their privacy policy.

## Changes to this privacy policy

I keep this privacy policy under regular review and place any updates on this web page. This privacy policy was last updated on the timestamped date indicated in the footer.  The privacy policy change history can be viewed [here](https://gitlab.com/1010/1010.gitlab.io/commits/master/content/privacy-notice/index.md).

## How to contact me

Mastodon: <https://fosstodon.org/@chris>
