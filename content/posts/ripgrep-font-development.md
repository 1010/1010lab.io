---
title: "Ripgrep Configuration for Font Development"
date: 2023-12-08T21:10:01-05:00
draft: false
summary: "A configuration for filtered ripgrep text searches on source file types used in font development."
slug: "ripgrep-font-development"

tags: ["ripgrep", "fonts", "search"]
---

[Ripgrep](https://github.com/BurntSushi/ripgrep) is a command line file search tool known for its speed on large codebases. Here's an approach to defining custom filtered ripgrep search types on [Adobe feature](https://adobe-type-tools.github.io/afdko/OpenTypeFeatureFileSpecification.html), [designspace](https://fonttools.readthedocs.io/en/latest/designspaceLib/index.html), [glyphs](https://handbook.glyphsapp.com/en/source-formats/), [glyphspackage](https://handbook.glyphsapp.com/en/source-formats/), and [UFO](https://unifiedfontobject.org/) font development source files.

## Create ripgrep Configuration File

Create a [ripgrep configuration file](https://github.com/BurntSushi/ripgrep/blob/master/GUIDE.md#configuration-file) on the path `~/.ripgreprc` and enter the following definitions:

```plaintext
# Custom types
--type-add=dspace:*.designspace
--type-add=fea:*.fea
--type-add=glyphs:*.glyphs
--type-add=gpack:*.{plist,glyph}
--type-add=plist:*.plist
--type-add=ufo:*.{glyf,plist,designspace,fea}
```

This configuration creates six new search types, automatically filtering ripgrep searches on the defined file extensions. Our new types are:

- `dspace` - *.designspace files
- `fea` - *.fea files
- `glyphs` - *.glyphs files
- `gpack` - glyphspackage file types: *.plist, *.glyph
- `plist` - *.plist files
- `ufo` - UFO file types: *.designspace, *.fea, *.glyf, & *.plist files

If you prefer a different set of type names, modify the name after the equal sign and before the colon on each line of the ripgrep configuration file.

## Export ripgrep Configuration Path Environment Variable

Export the configuration file path in an environment variable. Add the following line to your shell configuration rc file:

```plaintext
export RIPGREP_CONFIG_PATH=~/.ripgreprc
```

and run `source` on the shell configuration file. My zsh shell configuration file is on the path `~/.zshrc`, and I run it like this:

```plaintext
source ~/.zshrc
```

Modify the shell configuration file path with other shells (e.g., bash). Refer to your shell documentation for details.

The above source step is not necessary with new terminal launches.  You should only need to use it if your terminal is open at the time that you edit your shell configuration file.

## Use the Custom Filtered Search Types

Ripgrep searches recursively by default. Navigate to the root level of your source repository before you run the commands below. Use the custom search types with the `-t` or `--type` command line option. I'll use the short option in the examples below. Replace it with `--type=[TYPE]` syntax if you prefer the long option.

### Search Adobe feature files

```plaintext
rg -t fea [SEARCH TERM]
```

### Search designspace files

```plaintext
rg -t dspace [SEARCH TERM]
```

### Search Glyphs files

```plaintext
rg -t glyphs [SEARCH TERM]
```

### Search glyphspackage files

```plaintext
rg -t gpack [SEARCH TERM]
```

### Search plist files

```plaintext
rg -t plist [SEARCH TERM]
```

### Search UFO files

```plaintext
rg -t ufo [SEARCH TERM]
```

You may use these custom types with other ripgrep command line options. Refer to the [ripgrep documentation](https://github.com/BurntSushi/ripgrep#documentation-quick-links), command line help (`rg --help`), and man page (`man rg`) documentation for the complete set of search features to customize the simple examples above.
