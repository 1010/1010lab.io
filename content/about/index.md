---
title: "About"
date: 2020-01-04T22:26:41-05:00
draft: false
summary: "About the 1010 site"
hidden: true
---

👋 Hi!

I'm [Chris](https://github.com/chrissimpkins). I started this blog to share my thoughts on development topics and learnings.

You'll find an index of articles at <https://1010.gitlab.io>. The full content RSS feed is <https://1010.gitlab.io/index.xml>.

Thanks for reading!

## Find Me

### On Mastodon

- [**@chris@fosstodon.org**](https://fosstodon.org/@chris) - FOSS development topics
- [**@csimpkins@typo.social**](https://typo.social/@csimpkins) - font development, typography, and language topics
- [**@chris@sweetme.at**](https://sweetme.at/@chris) - my single person Mastodon instance where I discuss my general interest topics and tinker on Mastodon software

### On GitHub

My personal account is:

- **@**[**chrissimpkins**](https://github.com/chrissimpkins)

I manage the following GitHub orgs:

- **@**[**source-foundry**](https://github.com/source-foundry)
- **@**[**py-actions**](https://github.com/py-actions)
- **@**[**f-actions**](https://github.com/f-actions)

I'm a Google Fonts Program Manager and contribute to the googlefonts org repositories and the google/fonts repository:

- **@**[**googlefonts**](https://github.com/googlefonts)
- [**google/fonts**](https://github.com/google/fonts)

And I'm a member of the Rust Programming Language org, former member of the Rust Programming Language Learning Working Group, and contributor to the `rustc` compiler source and documentation.

## Licenses

### Content License

1010 is a free work, as explained in the [Definition of Free Cultural Works](https://freedomdefined.org/Definition). I release content (defined as text, images, and videos) under the [Creative Commons Attribution-ShareAlike 4.0 International license](https://creativecommons.org/licenses/by-sa/4.0/).

### Source Code License

Source code that I author and publish on the 1010 site is released under the [Apache License, v2.0](https://www.apache.org/licenses/LICENSE-2.0.html).

I will explicitly indicate licenses when I publish source from other authors.

## How To

### Report an Issue

Please use [the issue tracker](https://gitlab.com/1010/1010.gitlab.io/-/issues) on the blog source repository.

### Access 1010 Source Files

The blog source can be found on the GitLab repository at <https://gitlab.com/1010/1010.gitlab.io>.

#### Download source

Download the latest version of the master branch source by clicking [here](https://gitlab.com/1010/1010.gitlab.io/-/archive/master/1010.gitlab.io-master.zip).

#### Clone the source with git

Use the following command to clone the source with `git`:

```shell
git clone https://gitlab.com/1010/1010.gitlab.io.git
```

### Build the Site

1010 is a static web site that is compiled from Markdown sources with [Hugo](https://github.com/gohugoio/hugo) using the GitLab CI/CD pipeline. Install `hugo` and build the site locally by executing the following command in the root of the repository:

```shell
$ hugo
```

You'll find the compiled static site in the `public` directory.

### Contribute Fixes

Please submit [a new GitLab merge request](https://gitlab.com/1010/1010.gitlab.io/-/merge_requests) to propose minor changes / fixes.

## Privacy

I respect your privacy and do not collect data about you when you view the content on this site. Check out the [Privacy Notice](https://1010.gitlab.io/privacy-notice/) for details.

## Disclaimer

The opinions published in this blog are mine, and are not official statements or opinions of my employer or team.
