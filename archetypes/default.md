---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true

summary: ""

<!-- slug: "***" # Add slug for URL string -->
<!-- tags: ["Tag 1", "Tag 2", "Tag 3"] # Adds tags to the post -->
<!-- image: https://example.com/img/1/image.jpg # Cover used for OpenGraph and Twitter Cards -->
---

